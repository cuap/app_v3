# Stage 1: immagine per build
FROM python:3.11-alpine as builder

# Installa le dipendenze
COPY requirements.txt /tmp/
RUN pip install --no-cache-dir -r /tmp/requirements.txt

# Stage 2: immagine runtime
FROM scratch

# Copia le dipendenze installate
COPY --from=builder /usr/local/lib/python3.11 /usr/local/lib/python3.11
COPY --from=builder /usr/local/bin/python3.11 /usr/local/bin/python3.11
COPY --from=builder /lib/ld-musl-x86_64.so.1 /lib/ld-musl-x86_64.so.1
COPY --from=builder /usr/local/bin/../lib/libpython3.11.so.1.0 /usr/local/bin/../lib/libpython3.11.so.1.0
COPY --from=builder /lib/ld-musl-x86_64.so.1 /lib/ld-musl-x86_64.so.1
COPY --from=builder /lib/libz.so.1 /lib/libz.so.1 

# Copia l'app
COPY src /app/


# Expose port 5000
EXPOSE 5000
CMD ["/usr/local/bin/python3.11","/app/app.py"]
